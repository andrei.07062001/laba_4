package matrix

import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

class TestMatrixFilling {

    @Test
    fun `Dim 0`() {
        val m = mock(Matrix::class.java)
        try {
            MatrixFill(m, 0)
        } catch (ex: IllegalArgumentException) {
            print(ex.message)
        }
    }

    @Test
    fun `Dim lower than 0`() {
        val m = mock(Matrix::class.java)
        try {
            MatrixFill(m, -1)
        } catch (ex: IllegalArgumentException) {
            print(ex.message)
        }
    }

    @Test
    fun `Matrix 1 x 1`() {
        val m = mock(Matrix::class.java)
        MatrixFill(m, 1)
        Mockito.verify(m).set(0, 0, 1)
    }

    @Test
    fun `Matrix 3 x 3`() {
        val m = mock(Matrix::class.java)
        MatrixFill(m, 3)
        // 3,  2,  1,
        // 4,  7,  8,
        // 5,  6,  9
        Mockito.verify(m).set(0, 2, 1)
        Mockito.verify(m).set(0, 1, 2)
        Mockito.verify(m).set(0, 0, 3)
        Mockito.verify(m).set(1, 0, 4)
        Mockito.verify(m).set(2, 0, 5)
        Mockito.verify(m).set(2, 1, 6)
        Mockito.verify(m).set(1, 1, 7)
        Mockito.verify(m).set(1, 2, 8)
        Mockito.verify(m).set(2, 2, 9)
    }

    @Test
    fun `Matrix 4 x 4`() {
        val m = mock(Matrix::class.java)
        MatrixFill(m, 4)
        // 4,  3,  2,  1,
        // 5, 10, 11, 12,
        // 6,  9, 14, 13,
        // 7,  8, 15, 16
        Mockito.verify(m).set(0, 3, 1)
        Mockito.verify(m).set(0, 2, 2)
        Mockito.verify(m).set(0, 1, 3)
        Mockito.verify(m).set(0, 0, 4)
        Mockito.verify(m).set(1, 0, 5)
        Mockito.verify(m).set(2, 0, 6)
        Mockito.verify(m).set(3, 0, 7)
        Mockito.verify(m).set(3, 1, 8)
        Mockito.verify(m).set(2, 1, 9)
        Mockito.verify(m).set(1, 1, 10)
        Mockito.verify(m).set(1, 2, 11)
        Mockito.verify(m).set(1, 3, 12)
        Mockito.verify(m).set(2, 3, 13)
        Mockito.verify(m).set(2, 2, 14)
        Mockito.verify(m).set(3, 2, 15)
        Mockito.verify(m).set(3, 3, 16)
    }
}
