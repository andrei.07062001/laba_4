package matrix

/**
 * Interface for any object as matrix
 */
interface Matrix {
    /**
     * set value in cell with row number and column number
     * @param row - num of row of cell
     * @param column - num of column of cell
     * @param value - value to write in cell
     */
    fun set(row: Int, column: Int, value: Int)
}
