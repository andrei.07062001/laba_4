package matrix

/**
 * Matrix filling according rule
 * For each element from matrix, we allocate and create one cell in result matrix,
 * example of result matrix 5x5 filled by elements from 1 to 25:
 * 5,  4,  3,  2,  1,
 * 6, 13, 14, 15, 16
 * 7, 12, 19, 18, 17
 * 8, 11, 20, 23, 24,
 * 9, 10, 21, 22, 25
 * @param matrix - matrix where elements will be changed
 * @param dim - matrix dimension
 */
fun MatrixFill(matrix: Matrix, dim: Int) {
    if (dim <= 0) {
        throw IllegalArgumentException("Wrong dimension: it should be greater than 0")
    }
    val resultBuilder = AngleMatrixBuilder()
    resultBuilder.buildSize(dim)
    (1 until dim * dim + 1).forEach() {
        resultBuilder.buildCell(matrix)
    }
}
