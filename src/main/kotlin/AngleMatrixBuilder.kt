package matrix

import kotlin.math.pow
/**
 * Builder for our matrix
 */
class AngleMatrixBuilder {
    private var dim = 0
    private var counter: Int = 1
    private var branchNumber = 0
    private var row = 0
    private var column = 0
    private var i = 0
    private var j = 0
    private var sum = 0
    /**
     * Build body with length equal size, and calculate first sum of an arithmetic progression
     * @param dim dimension of our matrix
     */
    fun buildSize(dim: Int) {
        this.dim = dim
        sum = 2 * dim - 1
    }
    /**
     * Allocates a matrix cell based on the number of the current element and writes value there
     * @param matrix - matrix where elements will be changed
     */
    fun buildCell(matrix: Matrix) {
        if (counter > sum) {
            branchNumber++ // increment branch number if we got to the next branch
            sum += 2 * dim - 2 * (branchNumber) - 1 // recalculate the sum of the elements of an arithmetic progression
        }
        // We need this if to avoid looping to find the branchNumber
        // Formula counter = dim +(-1)^n * row + (-1)^(n+1) * column + 2nk - n^2 - n;
        // as n = branchNumber = min(row, column)
        j = branchNumber
        i = (counter - dim - (-1).toDouble().pow(branchNumber + 1).toInt() * j - 2 * branchNumber * dim + branchNumber * (branchNumber + 1)) * (-1).toDouble().pow(branchNumber).toInt()
        // We dont know i is row or column and this is why we need this if
        if (branchNumber <= i) {
            row = i
            column = j
        } else {
            row = j
            column = (counter - dim - (-1).toDouble().pow(branchNumber).toInt() * j - 2 * branchNumber * dim + branchNumber * (branchNumber + 1)) * (-1).toDouble().pow(branchNumber + 1).toInt()
        }
        matrix.set(row, column, counter)
        counter++
    }
}
